from random import randint

name = input(" Hi! What is your name?")

for guess_number in range (1,6):
    number_between_1_and_12 = randint(1, 12)
    number_between_1924_and_2004 = randint(1924, 2004)

    print(name, "Were you born in", number_between_1_and_12,"/", number_between_1924_and_2004)
    response = input("Yes or no?")

    if response == "Yes":
        print("Amazing!I got it right!")
        break
    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Darn! I got it wrong.")
